package es.manelcc.ciclodevida;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "DEPURACION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e(TAG, "ENTRO EN ONCREATE");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "entro en onRestart");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "entro en onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "entro en onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "entro en onPause");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "entro en onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "entro en onDestroy");
    }
}
